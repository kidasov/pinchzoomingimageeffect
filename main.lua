system.activate( "multitouch" )
display.setDefault( "anchorX", 0 )
display.setDefault( "anchorY", 0 )

centerX = display.contentCenterX
centerY = display.contentCenterY

screenWidth = display.actualContentWidth
screenHeight = display.actualContentHeight

local pageGroup
local myBox
local minXScale = 1
local minYScale = 1
local maxXScale = 4
local maxYScale = 4

-- math helpers
local sqrt  = math.sqrt
local abs     = math.abs
-- current touches
local touches      = {}

-- last zoom distance - I use this to compare with current distance
-- if its smaller then zoom out, else  zoom in
local lastDistance = 0
local zoomObject   = nil      --the zoom object
local OnZoomIn     = nil      --the zoom in event
local OnZoomOut    = nil      --the zoom out event
local x,y

--fictive touch
local fictiveTouch = {}
local fictiveCircle
local fictiveTouchX = display.contentWidth / 2
local fictiveTouchY = display.contentHeight / 2
local factor

fictiveTouch.id = "QHHJGL10"

-----------------------------------------------
-- counts a dictionary
-----------------------------------------------
local function countDictionary(dict)
    local ret = 0
    for k, v in pairs(dict) do
        ret = ret + 1
    end
    return ret
end

-----------------------------------------------
-- returns an element at some index in a dictionary
-----------------------------------------------
local function getDictElAtIndex( dict, index )
    local temp = 0
    local ret  = nil
    for k, v in pairs(dict) do
        if( temp == index ) then
            ret = v
            break
        end
        temp = temp + 1
    end
    return ret
end


-----------------------------------------------
-- update x and y for a specific touch-event
-----------------------------------------------
local function saveTouchCoords( event )
    local id  = event.id
    local target = event.target
    local  x  = event.x
    local  y  = event.y
    local xStart = x - target.x
    local yStart = y - target.y
    if touches[id] then
        touches[id].x = x
        touches[id].y = y
        touches[id].xStart = xStart
        touches[id].yStart = yStart
    end
end

-----------------------------------------------
-- calculate the distance between two touches
-----------------------------------------------
local function distanceBetweenTwoPoints( t1, t2 )
    local ret = 0
    local deltax = t2.x - t1.x
    local deltay = t2.y - t1.y
    ret = sqrt( deltax * deltax + deltay * deltay )
    return ret
end

-----------------------------------------------
-- for the simulator: create a fictive touch
-----------------------------------------------
function setFictiveSimulatorTouch( x, y)
  fictiveTouch.x = x; fictiveTouch.y = y
  touches[ fictiveTouch.id ] = fictiveTouch
  fictiveCircle = display.newCircle( fictiveTouch.x, fictiveTouch.y, 5)
end


-----------------------------------------------
-- for the simulator: removes a fictive touch
-----------------------------------------------
function removeFictiveSimulatorTouch()
  touches[ fictiveTouch.id ] = nil
end


local function onKeyEvent( event )
    -- Print which key was pressed down/up
    local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase
    if (event.keyName == "m") then
        setFictiveSimulatorTouch(fictiveTouchX, fictiveTouchY)--myBox.contentWidth - 100, 100)
        --setFictiveSimulatorTouch()
    elseif (event.keyName == "n") then
        removeFictiveSimulatorTouch()
    end

    if ( event.keyName == "back" ) then
        local platformName = system.getInfo( "platformName" )
        if ( platformName == "Android" ) or ( platformName == "WinPhone" ) then
            return true
        end
    end

    return false
end

local flag = 0

local screenX = 0
local screenY = 0

local targetBeginX = 0
local targetEndX = 0
local targetBeginY = 0
local targetEndY = 0

local newImageX = 0
local newImageY = 0

local it = 0
print('display.screenOriginY', display.screenOriginY)

local function handleTouchEvent(event)
    -- body
    local target = event.target

    local ret = false
    local phase = event.phase

    --
    local xMinOffset = -(pageGroup.contentWidth - (pageGroup.anchorX * pageGroup.contentWidth + (screenWidth - screenX ) + display.screenOriginX))
    local xMaxOffset = pageGroup.anchorX * pageGroup.contentWidth - screenX + display.screenOriginX
    local yMinOffset = -(pageGroup.contentHeight - (pageGroup.anchorY * pageGroup.contentHeight + (screenHeight - screenY + display.screenOriginY)))
    local yMaxOffset = pageGroup.anchorY * pageGroup.contentHeight - screenY + display.screenOriginY

    local xMin = screenX + xMinOffset
    local xMax = screenX +  xMaxOffset
    local yMin =  screenY + yMinOffset
    local yMax = screenY + yMaxOffset



    if (phase == "began") then

        target.xStart = event.x - target.x
        target.yStart = event.y - target.y

        touches[event.id] = event
        display.getCurrentStage():setFocus( event.target )
        event.target.isFocus = true

    elseif (phase == "moved") then

        saveTouchCoords(event)
        if (countDictionary(touches) >= 2) then
            local touch1 = getDictElAtIndex(touches, 0)
            local touch2 = getDictElAtIndex(touches, 1)

            local distance = distanceBetweenTwoPoints(touch1, touch2)

            if lastDistance ~= -1 then
                targetBeginX = event.x
                targetBeginY = event.y
                if (distance < lastDistance) then
                    -- check min scale factor
                    if (pageGroup.xScale > minXScale and pageGroup.yScale > minYScale) then
                        if (target.x < xMin) then
                            target.x = xMin
                        end

                        if (target.x > xMax) then
                            target.x = xMax
                        end

                        if (target.y < yMin) then
                            target.y = yMin
                        end

                        if (target.y > yMax) then
                            target.y = yMax
                        end
                        pageGroup.xScale = pageGroup.xScale / 1.01
                        pageGroup.yScale = pageGroup.yScale / 1.01

                        -- newImageX = newImageX / 1.01

                    end
                else
                    if (pageGroup.xScale < maxXScale and pageGroup.yScale < maxYScale) then
                        if (flag == 0) then

                            if (it == 0) then
                                newImageX = event.x - target.x
                                newImageY = event.y - target.y
                            end

                            if (it ~= 0) then
                                newImageX = pageGroup.anchorX * pageGroup.contentWidth - (targetEndX - targetBeginX)
                                newImageY = pageGroup.anchorY * pageGroup.contentHeight - (targetEndY - targetBeginY)
                            end

                            it = it + 1

                            -- find new anchor
                            pageGroup.anchorX =  newImageX / pageGroup.contentWidth
                            pageGroup.anchorY =  newImageY / pageGroup.contentHeight

                            flag = 1

                            screenX = event.x
                            screenY = event.y

                            -- set x, y coordinates of the group
                            -- we assign it to event x, y to prevent jumps.
                            pageGroup.x = screenX
                            pageGroup.y = screenY


                            local pageGroupPoint = display.newCircle(pageGroup.x, pageGroup.y, 3)

                        end

                        pageGroup.xScale = pageGroup.xScale * 1.01
                        pageGroup.yScale = pageGroup.yScale * 1.01

                        -- newImageX = newImageX * 1.01

                    end
                end

            end
            lastDistance = distance
        elseif (countDictionary(touches) == 1) then

            flag = 0
            target.x = event.x - target.xStart
            target.y = event.y - target.yStart



            if (target.x < xMin) then
                target.x = xMin
            end

            if (target.x > xMax) then
                target.x = xMax
            end

            if (target.y < yMin) then
                target.y = yMin
            end

            if (target.y > yMax) then
                target.y = yMax
            end

            targetEndX = target.x
            targetEndY = target.y

        end
    elseif (phase == "ended") then
        touches[event.id] = nil
        lastDistance = -1
        display.getCurrentStage():setFocus( nil )
        event.target.isFocus = false

    end

end


pageGroup = display.newGroup()
local mapImage = display.newImageRect("map_bg.jpg", 566, 426 )
mapImage.anchorX = 0
mapImage.anchorY = 0
mapImage.x = 0
mapImage.y = 0

pageGroup:insert(mapImage)

-- myBox = display.newRect(0, 0, 480*2, 320)
-- myBox.anchorY = 0n
-- myBox.xScale = 1
-- myBox.strokeWidth = 6


-- myBox:setFillColor( 1, 0, 0, 0.8 )
-- pageGroup:insert( myBox )

-- myBox2 = display.newRect(myBox.width / 2, myBox.height / 2, 50, 50 )
-- myBox2:setFillColor( 1, 1, 0, 0.8 )
-- myBox2.xScale = 1
-- pageGroup:insert( myBox2 )

-- myBox.anchorX = 0
-- myBox.anchorY = 0
-- myBox2.anchorX = 0.5
-- myBox2.anchorY = 0.5


pageGroup.x = display.screenOriginX




pageGroup.anchorChildren = true
pageGroup.anchorX = 0
pageGroup.anchorY = 0

local line = display.newLine(0, 0, 0, display.contentHeight)
line:setStrokeColor(0, 0, 0)
line.strokeWidth = 1
local line2 = display.newLine(display.contentWidth, 0, display.contentWidth, display.contentHeight)
line2:setStrokeColor(0, 0, 0)
line2.strokeWidth = 1
local line3 = display.newLine(display.contentWidth/2, 0, display.contentWidth/2, display.contentHeight)
line3:setStrokeColor(0, 0, 0)
line3.strokeWidth = 1

pageGroup:addEventListener("touch", handleTouchEvent)
Runtime:addEventListener("key", onKeyEvent)

